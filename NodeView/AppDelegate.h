//
//  AppDelegate.h
//  NodeView
//
//  Created by Brian Williams on 23/02/2013.
//  Copyright (c) 2013 Brian Williams. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
