//
//  main.m
//  NodeView
//
//  Created by Brian Williams on 23/02/2013.
//  Copyright (c) 2013 Brian Williams. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
